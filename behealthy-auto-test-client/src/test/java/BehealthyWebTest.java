import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestTemplateConfiguration.class)
public class BehealthyWebTest {

    @Autowired
    private BehealthyWebClient client;

    private String username = "TU-" + (int) (Math.random() * 100000);
    private String password = "1111";

    @Test
    public void workFlowTest() {
        client.register(username, password);
        String token = client.login(username, password);
        Assert.assertTrue(client.logout(token));
    }
}
