import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestTemplateConfiguration {

    @Bean
    public RestTemplateBuilder restTemplateBuilder() {
        return new RestTemplateBuilder();
    }

    @Bean
    public BehealthyWebClient behealthyWebClient(@Value("${behealthy.web.url}") String baseUrl,
                                                 RestTemplateBuilder builder) {
        return new BehealthyWebClient(baseUrl, builder);
    }
}
