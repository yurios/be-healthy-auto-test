import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class BehealthyWebClient {

    private static final String HTTP_BODY_USER_NAME = "name";
    private static final String HTTP_BODY_USER_PASSWORD = "password";

    // Path to the appropriates endpoints
    private static final String PATH_LOGOUT = "/logout";
    private static final String PATH_LOGIN = "/login";
    private static final String PATH_REGISTER = "/register";

    private final RestTemplate restTemplate;
    private final String baseUrl;

    public BehealthyWebClient(String baseUrl, RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        this.baseUrl = baseUrl;
    }

    public Map register(String name, String password) {
        HttpEntity<Map<String, String>> entity = new HttpEntity<>(credentialsMap(name, password));
        return restTemplate.exchange(baseUrl + PATH_REGISTER, HttpMethod.POST, entity, Map.class).getBody();
    }

    private Map<String, String> credentialsMap(String name, String password) {
        Map<String, String> credentials = new HashMap<>();
        credentials.put(HTTP_BODY_USER_NAME, name);
        credentials.put(HTTP_BODY_USER_PASSWORD, password);
        return credentials;
    }


    public String login(String name, String password) {
        HttpEntity<Map<String, String>> entity = new HttpEntity<>(credentialsMap(name, password));
        return restTemplate.exchange(baseUrl + PATH_LOGIN, HttpMethod.POST, entity, String.class).getBody();
    }

    public boolean logout(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, token);
        HttpEntity<?> entity = new HttpEntity<>(headers);
        ResponseEntity<Void> response = restTemplate.exchange(baseUrl + PATH_LOGOUT, HttpMethod.POST, entity, Void.class);
        return response.getStatusCode() == HttpStatus.OK;
    }
}
